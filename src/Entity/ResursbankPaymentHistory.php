<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use JsonException;

/**
 * NOTE: This class is named in a weird way to make sure it works with
 * Prestashop's internal Entity -> Table mapping
 * (Entity model -> _DB_PREFIX_ . [entity model class in snake casing]).
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Resursbank\OrderManagement\Repository\ResursbankPaymentHistoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ResursbankPaymentHistory
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="id", nullable=false, scale=10, options={"unsigned":true})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="order_id", nullable=false, scale=10, options={"unsigned":true})
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private $event;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $extra;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="state_from", nullable=true, length=255)
     */
    private $stateFrom;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="state_to", nullable=true, length=255)
     */
    private $stateTo;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    private $createdAt;

    /**
     * @var string
     */
    public const ENTITY_ID = 'id';

    /**
     * Relationship to Magento\Sales\Model\Order\Payment entity.
     *
     * @var string
     */
    public const ENTITY_ORDER_ID = 'order_id';

    /**
     * @var string
     */
    public const ENTITY_EVENT = 'event';

    /**
     * @var string
     */
    public const ENTITY_USER = 'user';

    /**
     * @var string
     */
    public const ENTITY_EXTRA = 'extra';

    /**
     * @var string
     */
    public const ENTITY_STATE_FROM = 'state_from';

    /**
     * @var string
     */
    public const ENTITY_STATE_TO = 'state_to';

    /**
     * @var string
     */
    public const ENTITY_CREATED_AT = 'created_at';

    /**
     * @var string
     */
    public const EVENT_CALLBACK_UNFREEZE = 'callback_unfreeze';

    /**
     * @var string
     */
    public const EVENT_CALLBACK_BOOKED = 'callback_booked';

    /**
     * @var string
     */
    public const EVENT_CALLBACK_UPDATE = 'callback_update';

    /**
     * @var string
     */
    public const EVENT_CAPTURE_CALLED = 'capture_called';

    /**
     * @var string
     */
    public const EVENT_CAPTURE_FAILED = 'capture_failed';

    /**
     * @var string
     */
    public const EVENT_CAPTURE_API_CALLED = 'capture_api_called';

    /**
     * @var string
     */
    public const EVENT_CANCEL_CALLED = 'cancel_called';

    /**
     * @var string
     */
    public const EVENT_CANCEL_FAILED = 'cancel_failed';

    /**
     * @var string
     */
    public const EVENT_CANCEL_API_CALLED = 'cancel_api_called';

    /**
     * @var string
     */
    public const EVENT_REFUND_CALLED = 'refund_called';

    /**
     * @var string
     */
    public const EVENT_REFUND_FAILED = 'refund_failed';

    /**
     * @var string
     */
    public const EVENT_REFUND_API_CALLED = 'refund_api_called';

    /**
     * @array
     */
    public const EVENT_LABELS = [
        self::EVENT_CALLBACK_BOOKED => 'Callback "Booked" received.',
        self::EVENT_CALLBACK_UNFREEZE => 'Callback "Unfreeze" received.',
        self::EVENT_CALLBACK_UPDATE => 'Callback "Update" received.',
        self::EVENT_CAPTURE_CALLED => 'Capture payment was called.',
        self::EVENT_CAPTURE_FAILED => 'Capture payment failed. Check the logs.',
        self::EVENT_CAPTURE_API_CALLED => 'Payment was debited at Resurs.',
        self::EVENT_CANCEL_CALLED => 'Cancel payment was called.',
        self::EVENT_CANCEL_FAILED => 'Cancel payment failed. Check the logs.',
        self::EVENT_CANCEL_API_CALLED => 'Payment was annulled at Resurs',
        self::EVENT_REFUND_CALLED => 'Refund payment was called.',
        self::EVENT_REFUND_FAILED => 'Refund payment failed. Check the logs.',
        self::EVENT_REFUND_API_CALLED => 'Payment was credited at Resurs.',
    ];

    /**
     * @var string
     */
    public const USER_CUSTOMER = 'customer';

    /**
     * @var string
     */
    public const USER_RESURS_BANK = 'resurs_bank';

    /**
     * @var string
     */
    public const USER_CLIENT = 'client';

    /**
     * @var array
     */
    public const USER_LABELS = [
        self::USER_CUSTOMER => 'Customer',
        self::USER_RESURS_BANK => 'Resurs Bank',
        self::USER_CLIENT => 'Client',
    ];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @param int $orderId
     *
     * @return ResursbankPaymentHistory
     */
    public function setOrderId(int $orderId): ResursbankPaymentHistory
    {
        if ($orderId === 0) {
            throw new InvalidArgumentException('Order ID not supplied.');
        }

        $this->orderId = $orderId;

        return $this;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @param string $event
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setEvent(
        string $event
    ): ResursbankPaymentHistory {
        if ($event === '') {
            throw new InvalidArgumentException('Event may not be empty.');
        }

        $this->event = $event;

        return $this;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setUser(
        string $user
    ): ResursbankPaymentHistory {
        if ($user === '') {
            throw new InvalidArgumentException('User may not be empty.');
        }

        $this->user = $user;

        return $this;
    }

    /**
     * @return array
     *
     * @throws JsonException
     */
    public function getExtra(): array
    {
        return json_decode($this->extra ?? '{}', true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param array $extra
     *
     * @return $this
     *
     * @throws JsonException
     */
    public function setExtra(
        array $extra
    ): ResursbankPaymentHistory {
        $this->extra = json_encode($extra, JSON_THROW_ON_ERROR);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStateFrom(): ?string
    {
        return $this->stateFrom;
    }

    /**
     * @param string|null $stateFrom
     *
     * @return $this
     *
     */
    public function setStateFrom(
        ?string $stateFrom
    ): ResursbankPaymentHistory {
        $this->stateFrom = $stateFrom;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStateTo(): ?string
    {
        return $this->stateTo;
    }

    /**
     * @param string|null $stateTo
     *
     * @return $this
     */
    public function setStateTo(
        ?string $stateTo
    ): ResursbankPaymentHistory {
        $this->stateTo = $stateTo;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
}
