<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Config\Form;

use PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface;
use PrestaShopBundle\Service\Routing\Router;
use Resursbank\OrderManagement\Config\Form\Builder\Callback;
use Resursbank\OrderManagement\Config\Form\Builder\General;
use Resursbank\OrderManagement\Config\Form\Builder\StateMapping;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Configuration form builder.
 */
class Builder
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var FormDataProviderInterface
     */
    private $formDataProvider;

    /**
     * @var Router
     */
    private $router;

    /**
     * @param FormFactoryInterface $formFactory
     * @param FormDataProviderInterface $formDataProvider
     * @param Router $router
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        FormDataProviderInterface $formDataProvider,
        Router $router
    ) {
        $this->formFactory = $formFactory;
        $this->formDataProvider = $formDataProvider;
        $this->router = $router;
    }

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->formFactory->createBuilder()
            ->setData($this->formDataProvider->getData())
            ->add('general', General::class)
            ->add('callback', Callback::class)
            ->add('state_mapping', StateMapping::class)
            ->setAction(
                $this->router->generate(
                    'resursbank_ordermanagement_admin_config_save'
                )
            )->getForm();
    }
}
