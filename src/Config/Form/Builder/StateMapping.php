<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Config\Form\Builder;

use PrestaShop\PrestaShop\Core\Form\ChoiceProvider\OrderStateByIdChoiceProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Generates the form containing state (order status) settings.
 */
class StateMapping extends AbstractType
{
    /**
     * @var OrderStateByIdChoiceProvider
     */
    private $orderStateByIdChoiceProvider;

    /**
     * @param OrderStateByIdChoiceProvider $orderStateByIdChoiceProvider
     */
    public function __construct(
        OrderStateByIdChoiceProvider $orderStateByIdChoiceProvider
    ) {
        $this->orderStateByIdChoiceProvider = $orderStateByIdChoiceProvider;
    }

    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {
        $builder
            ->add('pending', ChoiceType::class, [
                'label' => 'Pending',
                'choices' => $this->orderStateByIdChoiceProvider->getChoices(),
                'required' => false,
            ])->add('processing', ChoiceType::class, [
                'label' => 'Processing',
                'choices' => $this->orderStateByIdChoiceProvider->getChoices(),
                'required' => false,
            ])->add('annulled', ChoiceType::class, [
                'label' => 'Annulled',
                'choices' => $this->orderStateByIdChoiceProvider->getChoices(),
                'required' => false,
            ])->add('completed', ChoiceType::class, [
                'label' => 'Completed',
                'choices' => $this->orderStateByIdChoiceProvider->getChoices(),
                'required' => false,
            ])->add('credited', ChoiceType::class, [
                'label' => 'Credited',
                'choices' => $this->orderStateByIdChoiceProvider->getChoices(),
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function getBlockPrefix(): string
    {
        return 'resursbank_ordermanagement_state_mapping';
    }
}
