<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Config\Form\Builder;

use PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Generates the form containing general order management settings.
 */
class General extends AbstractType
{
    /**
     * @var FormChoiceProviderInterface
     */
    private $stateChoiceProvider;

    /**
     * @param FormChoiceProviderInterface $stateChoiceProvider
     */
    public function __construct(
        FormChoiceProviderInterface $stateChoiceProvider
    ) {
        $this->stateChoiceProvider = $stateChoiceProvider;
    }

    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {
        $builder
            ->add('enabled', ChoiceType::class, [
                'label' => 'Enabled',
                'choices' => $this->stateChoiceProvider->getChoices(),
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function getBlockPrefix(): string
    {
        return 'resursbank_ordermanagement_general';
    }
}
