<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Config\Form\Builder;

use PrestaShopBundle\Service\Routing\Router;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Generates the form containing callback specific settings.
 */
class Callback extends AbstractType
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @param Router $router
     */
    public function __construct(
        Router $router
    ) {
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {
        $builder
            ->add('update', ButtonType::class, [
                'label' => 'Register',
                'attr' => [
                    'class' => 'btn-outline-primary pull-right',
                    'onClick' => sprintf(
                        "window.location = '%s'",
                        $this->router->generate(
                            'resursbank_ordermanagement_admin_config_callback_registration'
                        )
                    ),
                ],
            ])
            ->add('perform_test', ButtonType::class, [
                'label' => 'Test Callbacks',
                'attr' => [
                    'class' => 'btn-outline-primary pull-right',
                    'onClick' => sprintf(
                        "window.location = '%s'",
                        $this->router->generate(
                            'resursbank_ordermanagement_admin_config_callback_test'
                        )
                    ),
                ],
            ])
            ->add('last_triggered_at', TextType::class, [
                'label' => 'Last triggered at',
                'disabled' => true,
            ])
            ->add('last_received_at', TextType::class, [
                'label' => 'Last received at',
                'disabled' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function getBlockPrefix(): string
    {
        return 'resursbank_ordermanagement_callback';
    }
}
