<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Config;

use Exception;
use PrestaShop\PrestaShop\Adapter\Configuration as Adapter;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;

/**
 * Interact with the configuration table. This class provide methods to read and
 * write all values related to our module.
 */
class Config
{
    private const CONFIG_PREFIX = 'RESURSBANK_ORDERMANAGEMENT_';
    public const ENABLED = self::CONFIG_PREFIX . 'ENABLED';
    public const PENDING_ORDER_STATE = self::CONFIG_PREFIX . 'PENDING_ORDER_STATE';
    public const PROCESSING_ORDER_STATE = self::CONFIG_PREFIX . 'PROCESSING_ORDER_STATE';
    public const COMPLETED_ORDER_STATE = self::CONFIG_PREFIX . 'COMPLETED_ORDER_STATE';
    public const ANNULLED_ORDER_STATE = self::CONFIG_PREFIX . 'ANNULLED_ORDER_STATE';
    public const CREDITED_ORDER_STATE = self::CONFIG_PREFIX . 'CREDITED_ORDER_STATE';
    public const CALLBACK_LAST_TRIGGERED_AT = self::CONFIG_PREFIX . 'CALLBACK_LAST_TRIGGERED_AT';
    public const CALLBACK_LAST_RECEIVED_AT = self::CONFIG_PREFIX . 'CALLBACK_LAST_RECEIVED_AT';

    /**
     * @var Adapter
     */
    private $adapter;

    /**
     * @param Adapter $adapter
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return bool
     */
    public function isEnabled(
        ShopConstraint $shopConstraint = null
    ): bool {
        return (bool) $this->adapter->get(
            self::ENABLED,
            false,
            $shopConstraint
        );
    }

    /**
     * @param string $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setEnabled(
        string $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(self::ENABLED, (bool) $value, $shopConstraint);
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return string
     */
    public function getCallbackLastTriggeredAt(
        ShopConstraint $shopConstraint = null
    ): string {
        return (string) $this->adapter->get(
            self::CALLBACK_LAST_TRIGGERED_AT,
            false,
            $shopConstraint
        );
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setCallbackLastTriggeredAt(
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(
            self::CALLBACK_LAST_TRIGGERED_AT,
            date('Y-m-d H:i:s'),
            $shopConstraint
        );
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return string
     */
    public function getCallbackReceivedAt(
        ShopConstraint $shopConstraint = null
    ): string {
        return (string) $this->adapter->get(
            self::CALLBACK_LAST_RECEIVED_AT,
            '',
            $shopConstraint
        );
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setCallbackLastReceivedAt(
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(
            self::CALLBACK_LAST_RECEIVED_AT,
            date('Y-m-d H:i:s'),
            $shopConstraint
        );
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return int
     */
    public function getPendingState(
        ShopConstraint $shopConstraint = null
    ): int {
        return (int) $this->adapter->get(
            self::PENDING_ORDER_STATE,
            0,
            $shopConstraint
        );
    }

    /**
     * @param int $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setPendingState(
        int $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(
            self::PENDING_ORDER_STATE,
            $value,
            $shopConstraint
        );
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return int
     */
    public function getProcessingState(
        ShopConstraint $shopConstraint = null
    ): int {
        return (int) $this->adapter->get(
            self::PROCESSING_ORDER_STATE,
            0,
            $shopConstraint
        );
    }

    /**
     * @param int $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setProcessingState(
        int $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(
            self::PROCESSING_ORDER_STATE,
            $value,
            $shopConstraint
        );
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return int
     */
    public function getAnnulledState(
        ShopConstraint $shopConstraint = null
    ): int {
        return (int) $this->adapter->get(
            self::ANNULLED_ORDER_STATE,
            0,
            $shopConstraint
        );
    }

    /**
     * @param int $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setAnnulledState(
        int $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(
            self::ANNULLED_ORDER_STATE,
            $value,
            $shopConstraint
        );
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return int
     */
    public function getCompletedState(
        ShopConstraint $shopConstraint = null
    ): int {
        return (int) $this->adapter->get(
            self::COMPLETED_ORDER_STATE,
            0,
            $shopConstraint
        );
    }

    /**
     * @param int $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setCompletedState(
        int $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(
            self::COMPLETED_ORDER_STATE,
            $value,
            $shopConstraint
        );
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return int
     */
    public function getCreditedState(
        ShopConstraint $shopConstraint = null
    ): int {
        return (int) $this->adapter->get(
            self::CREDITED_ORDER_STATE,
            0,
            $shopConstraint
        );
    }

    /**
     * @param int $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setCreditedState(
        int $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set(
            self::CREDITED_ORDER_STATE,
            $value,
            $shopConstraint
        );
    }
}
