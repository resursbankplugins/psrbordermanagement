<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Model;

/**
 * Resurs Bank order state index.
 */
class OrderStates
{
    public const PAYMENT_REVIEW = 'PS_RB_OS_PAYMENT_REVIEW';

    /**
     * As said in an early state, we can not do what PrestaShop is doing with its processing state
     * as this is by PrestaShop considered a paid status. By all means, this also causes PrestaShop to
     * double the payment amount each time Resurs Bank might send the callbacks wrongly or doing it
     * too fast, causing race conditions between the http requests.
     *
     * @var string
     * @since 1.0.0
     * @see https://test.resurs.com/docs/x/GYCJAw#PrestaShopSimplifiedShopFlow-ResursBankpluginneedsownstatusesforpending/processingorders-why?
     */
    public const PAYMENT_PROCESSING = 'PS_RB_OS_PAYMENT_PROCESSING';

    /**
     * @return array[]
     */
    public static function states(): array
    {
        return [
             self::PAYMENT_REVIEW => [
                 'name' => 'Resurs Bank - Payment Review',
                 'send_email' => 0,
                 'invoice' => 0,
                 'color' => '#e1be00',
                 'unremovable' => 1,
                 'hidden' => 1,
                 'logable' => 1,
                 'paid' => 0,
             ],
             self::PAYMENT_PROCESSING => [
                 'name' => 'Resurs Bank - Confirmed',
                 'send_email' => 0,
                 'invoice' => 0,
                 'color' => '#009b96',
                 'unremovable' => 1,
                 'hidden' => 1,
                 'logable' => 1,
                 'paid' => 0,
             ],
        ];
    }
}
