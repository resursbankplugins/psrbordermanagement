<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Controller;

use function constant;
use Doctrine\ORM\ORMException;
use Exception;
use JsonException;
use Order as PsOrder;
use PrestaShop\PrestaShop\Adapter\Entity\Order;
use PrestaShop\PrestaShop\Adapter\Entity\OrderState;
use PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException;
use PrestaShopDatabaseException;
use PrestaShopException;
use Resursbank\Core\Api\Connection;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\Core\Repository\ResursbankOrderRepository;
use Resursbank\Core\Entity\ResursbankOrder;
use Resursbank\Ecommerce\Types\OrderStatus;
use Resursbank\OrderManagement\Config\Config;
use Resursbank\OrderManagement\Entity\ResursbankPaymentHistory;
use Resursbank\OrderManagement\Exception\CallbackValidationException;
use Resursbank\OrderManagement\Exception\CallbacksUnavailableException;
use Resursbank\OrderManagement\Exception\OrderManagementNotEnabledException;
use Resursbank\OrderManagement\Exception\OrderNotFoundException;
use Resursbank\OrderManagement\Exception\ResolveOrderStatusFailedException;
use Resursbank\OrderManagement\Repository\ResursbankPaymentHistoryRepository;

/**
 * Business logic for callback integration.
 */
class Callback
{
    /**
     * @var LoggerInterface
     */
    private $callbackLogger;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ResursbankPaymentHistoryRepository
     */
    private $paymentHistoryRepository;

    /**
     * @var ResursbankOrderRepository
     */
    private $orderRepository;

    /**
     * @var string
     */
    private $secret;

    /**
     * @param LoggerInterface $logger
     * @param LoggerInterface $callbackLogger
     * @param Config $config
     * @param Connection $connection
     * @param ResursbankPaymentHistoryRepository $paymentHistoryRepository
     * @param ResursbankOrderRepository $orderRepository
     * @param string $secret
     */
    public function __construct(
        LoggerInterface $logger,
        LoggerInterface $callbackLogger,
        Config $config,
        Connection $connection,
        ResursbankPaymentHistoryRepository $paymentHistoryRepository,
        ResursbankOrderRepository $orderRepository,
        string $secret
    ) {
        $this->callbackLogger = $callbackLogger;
        $this->config = $config;
        $this->connection = $connection;
        $this->logger = $logger;
        $this->paymentHistoryRepository = $paymentHistoryRepository;
        $this->orderRepository = $orderRepository;
        $this->secret = $secret;
    }

    /**
     * @param string $paymentId
     * @param string $digest
     *
     * @return void
     *
     * @throws Exception
     */
    public function unfreeze(
        string $paymentId,
        string $digest
    ): void {
        $this->execute('unfreeze', $paymentId, $digest);
    }

    /**
     * @param string $paymentId
     * @param string $digest
     *
     * @return void
     *
     * @throws Exception
     */
    public function booked(
        string $paymentId,
        string $digest
    ): void {
        $this->execute('booked', $paymentId, $digest);
    }

    /**
     * @param string $paymentId
     * @param string $digest
     *
     * @return void
     *
     * @throws Exception
     */
    public function update(
        string $paymentId,
        string $digest
    ): void {
        $this->execute('update', $paymentId, $digest);
    }

    /**
     * @return void
     *
     * @throws Exception
     */
    public function test(): void
    {
        $this->logIncoming('test', '', '');

        $this->config->setCallbackLastReceivedAt();
    }

    /**
     * General callback instructions.
     *
     * @param string $type
     * @param string $orderReference
     * @param string $digest
     *
     * @return void
     *
     * @throws CallbackValidationException
     * @throws ORMException
     * @throws OrderManagementNotEnabledException
     * @throws OrderNotFoundException
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws JsonException
     * @throws ShopException
     */
    private function execute(
        string $type,
        string $orderReference,
        string $digest
    ): void {
        /* @noinspection BadExceptionsProcessingInspection */
        try {
            $this->validate($orderReference, $digest);
            $this->logIncoming($type, $orderReference, $digest);

            /** @var Order $order */
            /** @phpstan-ignore-next-line */
            $order = Order::getByReference($orderReference)->getFirst();

            if (!is_object($order)) {
                /*
                 * If $order is not an object (instanceof does not do the job properly) we should consider
                 * this order as "not ours", using a traceable error code.
                 */
                throw new OrderNotFoundException(
                    'Order not found.',
                    410
                );
            }


            if (!$order->id) {
                /*
                 * Using 410 (Gone) to mark none existing order.
                 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
                 */
                throw new OrderNotFoundException(
                    'Failed to locate order ' . $orderReference,
                    410
                );
            }

            // Verify that callbacks are enabled on order
            if (!$this->orderReadyForCallback($order)) {
                $this->logger->info("Not ready for callback");
                throw new CallbacksUnavailableException(
                    'Callbacks not available for order '. $orderReference,
                    410
                );
            }


            $oldState = $order->getCurrentOrderState();
            // This row tend to sometimes duplicate order statuses, rather than be happy
            // with the first state it gets. Please see syncStateFromResurs for similar information.
            $newState = new OrderState($this->syncStateFromResurs($order, $oldState, $type));

            $this->callbackLogger->info(
                "[{$type}] - PaymentId: {$orderReference}. Old State: {$oldState->id}, Expected New State: {$newState->id}."
            );

/*            if ($oldState !== null && $newState->id === $oldState->id) {
                $this->callbackLogger->info(
                    "[{$type}] - PaymentId: {$orderReference}. Expected state ignored due to identical setup."
                );
                return;
            }*/

            $historyEvent = constant(sprintf(
                '%s::%s',
                ResursbankPaymentHistory::class,
                'EVENT_CALLBACK_' . strtoupper($type)
            ));

            $this->paymentHistoryRepository->create(
                $order->id,
                $historyEvent,
                ResursbankPaymentHistory::USER_RESURS_BANK,
                $oldState->name[$order->id_lang] ?? 'NULL',
                $newState->name[$order->id_lang],
                []
            );
        } catch (Exception $e) {
            $this->logger->exception($e);
            throw $e;
        }
    }

    /**
     * Validate that the order can in fact handle callbacks.
     *
     * @param PsOrder $order
     * @return bool
     * @throws Exception
     */
    private function orderReadyForCallback(PsOrder $order): bool
    {
        $this->logger->info("Checking if callbacks enabled");
        $orderEntity = $this->orderRepository->findOneBy(['orderId' => $order->id]);

        try {
            return $orderEntity->getReadyForCallback();
        } catch (Exception $e) {
            $this->logger->exception($e);
            throw $e;
        }
    }

    /**
     * Validate the digest.
     *
     * @param string $paymentId
     * @param string $digest
     *
     * @throws CallbackValidationException
     */
    private function validate(
        string $paymentId,
        string $digest
    ): void {
        $ourDigest = strtoupper(
            sha1($paymentId . $this->secret)
        );

        if ($ourDigest !== $digest) {
            throw new CallbackValidationException(
                "Invalid digest - PaymentId: {$paymentId}. Digest: {$digest}",
                406
            );
        }
    }

    /**
     * Resolve state for the order by asking Resurs Bank.
     *
     * @param Order $order
     * @param $oldState
     *
     * @return int
     *
     * @throws PrestaShopException
     * @throws ResolveOrderStatusFailedException
     * @throws ShopException
     * @throws Exception
     */
    private function syncStateFromResurs(
        PsOrder $order,
        $oldState,
        string $type
    ): int {
        $connection = $this->connection->getConnection(
            $this->connection->getCredentialsFromOrder($order)
        );

        $state = $connection->getOrderStatusByPayment($order->reference);
        $newState = $this->mapStateFromResurs($state);

        // This procedure tend to sometimes duplicate order statuses, rather than be happy
        // with the first state it gets. This is why we also pass the oldState into this section a second time
        // since "newState" above sometimes already is the same as the oldState. By saving the data once
        // more, we raise the risks of doubling the invoice amount.
        if ($oldState !== null &&
            $newState !== $oldState->id
        ) {
            // Finalizations: Allow status updates to pass through, but leave the status change untouched
            // as long as the type is not matching "update".
            if ($state === OrderStatus::COMPLETED && $type !== 'update') {
                return $oldState->id;
            }
            $order->setCurrentState($newState);
            $order->save();
        }

        return $newState;
    }

    /**
     * Get the new order state based on constants from eCom.
     *
     * @param int $status
     *
     * @return int
     *
     * @throws ResolveOrderStatusFailedException
     */
    private function mapStateFromResurs(
        int $status
    ): int {
        switch ($status) {
            case OrderStatus::PENDING:
                $orderState = $this->config->getPendingState();
                break;
            case OrderStatus::PROCESSING:
                $orderState = $this->config->getProcessingState();
                break;
            case OrderStatus::COMPLETED:
                $orderState = $this->config->getCompletedState();
                break;
            case OrderStatus::ANNULLED:
                $orderState = $this->config->getAnnulledState();
                break;
            case OrderStatus::CREDITED:
                $orderState = $this->config->getCreditedState();
                break;
            default:
                throw new ResolveOrderStatusFailedException(
                    sprintf(
                        'Failed to resolve order status (%s) from Resurs Bank.',
                        $status
                    )
                );
        }

        return $orderState;
    }

    /**
     * Log incoming callbacks.
     *
     * @param string $type
     * @param string $paymentId
     * @param string $digest
     */
    private function logIncoming(
        string $type,
        string $paymentId,
        string $digest
    ): void {
        $this->callbackLogger->info(
            "[{$type}] - PaymentId: {$paymentId}. Digest: {$digest}"
        );
    }
}
