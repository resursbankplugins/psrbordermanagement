<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Controller\Admin\Config;

use Exception;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\OrderManagement\Config\Form\Builder;
use Resursbank\OrderManagement\Service\Callback;
use Symfony\Component\HttpFoundation\Response;

/**
 * Render configuration page.
 */
class View extends FrameworkBundleAdminController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Builder
     */
    private $builder;
    /**
     * @var Callback
     */
    private $callback;

    /**
     * @var \psrbordermanagement
     */
    private $module;

    /**
     * @param LoggerInterface $logger
     * @param Builder $builder
     */
    public function __construct(
        LoggerInterface $logger,
        Builder $builder,
        Callback $callback
    ) {
        parent::__construct();
        $this->logger = $logger;
        $this->builder = $builder;
        $this->callback = $callback;
        $this->module = \Module::getInstanceByName('psrbordermanagement');
    }

    /**
     * @return Response|null
     *
     * @throws Exception
     */
    public function execute(): ?Response
    {
        $properUrlNotice = '';
        try {
            $callbackList = $this->callback->fetch();

            if (is_array($callbackList) && count($callbackList)) {
                $httpsCallbacks = 0;
                foreach ($callbackList as $item) {
                    if (isset($item->uriTemplate) && preg_match('/^https/i', $item->uriTemplate)) {
                        $httpsCallbacks++;
                    }
                }
                $properUrlNotice = $httpsCallbacks !== count($callbackList) ?  $this->module->l(
                    'Please note that the callbacks above may be incompatible with Resurs Bank that requires them in https format.'
                ) : '';
            }

            return $this->render(
                '@Modules/psrbordermanagement/views/admin/config.twig',
                [
                    'layoutTitle' => $this->module->l('Resurs Bank - Order Management'),
                    'requireAddonsSearch' => true,
                    'enableSidebar' => true,
                    'help_link' => '',
                    'form' => $this->builder->getForm()->createView(),
                    'callbacks' => $callbackList,
                    'properUrlNotice' => $properUrlNotice
                ]
            );
        } catch (Exception $e) {
            $this->logger->exception($e);
            throw $e;
        }
    }
}
