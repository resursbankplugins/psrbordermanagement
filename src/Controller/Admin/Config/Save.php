<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Controller\Admin\Config;

use Exception;
use function is_array;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Resursbank\Core\Exception\ConfigException;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\OrderManagement\Config\Form\DataProvider;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Save configuration settings.
 */
class Save extends FrameworkBundleAdminController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DataProvider
     */
    private $dataProvider;

    /**
     * @var \psrbordermanagement
     */
    private $module;

    /**
     * @param LoggerInterface $logger
     * @param DataProvider $dataProvider
     */
    public function __construct(
        LoggerInterface $logger,
        DataProvider $dataProvider
    ) {
        parent::__construct();

        $this->logger = $logger;
        $this->dataProvider = $dataProvider;
        $this->module = \Module::getInstanceByName('psrbordermanagement');
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function execute(
        Request $request
    ): RedirectResponse {
        /* @noinspection BadExceptionsProcessingInspection */
        try {
            $data = $request->get('form');

            if (!is_array($data)) {
                throw new ConfigException('Invalid or missing form data.');
            }

            $errors = $this->dataProvider->setData($data);

            if (count($errors) > 0) {
                $this->flashErrors($errors);
            } else {
                $this->addFlash(
                    'success',
                    $this->module->l('Successful update.')
                );
            }
        } catch (Exception $e) {
            // @todo Not sure if this is really safe?
            $this->addFlash('error', $e->getMessage());
            $this->logger->exception($e);
        }

        return $this->redirectToRoute('resursbank_ordermanagement_admin_config');
    }
}
