<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Controller;

use Exception;
use function is_string;
use JsonException;
use PrestaShop\PrestaShop\Adapter\Entity\ModuleFrontController;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use PrestaShopException;
use Resursbank\Core\Logger\Logger;

/**
 * General functionality for callback controllers.
 */
class FrontController extends ModuleFrontController
{
    /**
     * NOTE: Return type is specified inaccurately to suppress an unnecessary
     * error from PHP CS Fixer.
     *
     * @return callback
     *
     * @throws Exception
     */
    protected function getCallbackService(): Callback
    {
        $service = $this->get('resursbank.ordermanagement.controller.callback');

        if (!($service instanceof Callback)) {
            throw new Exception('Unexpected callback service.');
        }

        return $service;
    }

    /**
     * @return Logger
     *
     * @throws Exception
     */
    protected function getLoggerService(): Logger
    {
        $service = $this->get('resursbank.ordermanagement.callback.logger');

        if (!($service instanceof Logger)) {
            throw new Exception('Unexpected logger service.');
        }

        return $service;
    }

    /**
     * Resolve digest from request.
     *
     * @return string
     *
     * @throws Exception
     */
    protected function getDigest(): string
    {
        $digest = Tools::getValue('digest');

        if (!is_string($digest) || $digest === '') {
            throw new Exception('Improper or missing digest key.');
        }

        return $digest;
    }

    /**
     * Resolve payment id from request.
     *
     * @return string
     *
     * @throws Exception
     */
    protected function getPaymentId(): string
    {
        $paymentId = Tools::getValue('paymentId');

        if (!is_string($paymentId) || $paymentId === '') {
            throw new Exception('Improper or missing Payment ID.');
        }

        return $paymentId;
    }

    /**
     * @param string $message
     * @param int $code
     *
     * @throws PrestaShopException
     * @throws JsonException
     */
    protected function failedResponse(string $message, int $code): void
    {
        // Respond with code 500 if nothing else is defined.
        $this->respond($message, $code ? $code : 500);
    }

    /**
     * @throws PrestaShopException
     * @throws JsonException
     */
    protected function successResponse(): void
    {
        $this->respond('', 204);
    }

    /**
     * @param string $message
     * @param int $code
     *
     * @return void
     *
     * @throws JsonException
     * @throws PrestaShopException
     */
    private function respond(
        string $message,
        int $code
    ): void {
        header('Content-Type: application/json');
        http_response_code($code ? $code : 204);
        $this->ajaxRender(json_encode([
            'success' => $code === 204,
            'message' => $message,
            'code' => $code ? $code : 204,
        ], JSON_THROW_ON_ERROR));
        // After responding, just exit and stop PrestaShop further processing things that it should
        // not get involved with (discovered a SmartyException at this point that is not necessary to pass through).
        exit;
    }
}
