<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Service;

use JsonException;
use Order;
use PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException;
use Resursbank\Core\Api\Connection;
use Resursbank\Core\Exception\InvalidDataException;
use ResursException;
use TorneLIB\Exception\ExceptionHandler;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Renders payment information widget on backoffice order view.
 */
class PaymentInfo
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var Environment
     */
    private $twig;
    /**
     * @var OrderManagement
     */
    private $orderManagement;

    /**
     * @param Connection $connection
     * @param Environment $twig
     * @param OrderManagement $orderManagement
     */
    public function __construct(
        Connection $connection,
        Environment $twig,
        OrderManagement $orderManagement
    ) {
        $this->connection = $connection;
        $this->twig = $twig;
        $this->orderManagement = $orderManagement;
    }

    /**
     * Render the widget.
     *
     * @param Order $order
     *
     * @return string
     *
     * @throws InvalidDataException
     * @throws ShopException
     * @throws JsonException
     * @throws ResursException
     * @throws ExceptionHandler
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(
        Order $order
    ): string {
        $result = '';

        if ($this->orderManagement->isResursOrder($order)) {
            $data = $this->connection->getPayment($order);

            if ($data === null) {
                throw new InvalidDataException(
                    'Missing payment data for ' . $order->reference
                );
            }

            $data = json_decode(
                json_encode($data, JSON_THROW_ON_ERROR),
                true,
                512,
                JSON_THROW_ON_ERROR
            );

            $data['address'] = $this->getAddress($data);
            $data['status'] = isset($data['status']) ? (array) $data['status'] : [];

            $result = $this->twig->render(
                '@Modules/psrbordermanagement/views/templates/order/payment_info.twig',
                $data
            );
        }

        return $result;
    }

    /**
     * Returns the customer address as an ordered array. We will render this in
     * the widget template the same way the array is organized.
     *
     * @return string[]
     */
    private function getAddress(
        array $data
    ): array {
        $result = [];

        $address = $data['customer']['address'] ?? null;

        if (is_array($address)) {
            if ($this->hasAddressProperty($address, 'addressRow1')) {
                $result[] = $address['addressRow1'];
            }

            if ($this->hasAddressProperty($address, 'addressRow2')) {
                $result[] = $address['addressRow2'];
            }

            if ($this->hasAddressProperty($address, 'postalArea')) {
                $result[] = $address['postalArea'];
            }

            if ($this->hasAddressProperty($address, 'country') &&
                $this->hasAddressProperty($address, 'postalCode')
            ) {
                $result[] = "${address['country']} - ${address['postalCode']}";
            }
        }

        return $result;
    }

    /**
     * @param array $data
     * @param string $key
     *
     * @return bool
     */
    private function hasAddressProperty(
        array $data,
        string $key
    ): bool {
        return isset($data[$key]) && is_string($data[$key]);
    }
}
