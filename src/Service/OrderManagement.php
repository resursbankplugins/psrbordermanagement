<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Service;

use Context;
use Doctrine\ORM\ORMException;
use Employee;
use Exception;
use JsonException;
use PrestaShop\PrestaShop\Adapter\Entity\Order;
use PrestaShop\PrestaShop\Core\Domain\Order\Payment\Exception\PaymentException;
use PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use Resursbank\Core\Api\Connection;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\Core\Model\Api\Payment\Converter\Item\AbstractItem;
use Resursbank\Core\Model\Api\Payment\Item;
use Resursbank\OrderManagement\Config\Config;
use Resursbank\OrderManagement\Entity\ResursbankPaymentHistory;
use Resursbank\OrderManagement\Model\Api\Payment\Converter\RefundConverter;
use Resursbank\OrderManagement\Repository\ResursbankPaymentHistoryRepository;
use Resursbank\RBEcomPHP\ResursBank;
use ResursException;
use Tools;
use TorneLIB\Exception\ExceptionHandler;

/**
 * Business logic to process AfterShop commands (like capturing, refunding and
 * cancelling).
 */
class OrderManagement
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ResursbankPaymentHistoryRepository
     */
    private $paymentHistoryRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $employeeName;

    /**
     * @var RefundConverter
     */
    private $refundConverter;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Connection $connection
     * @param ResursbankPaymentHistoryRepository $paymentHistoryRepository
     * @param RefundConverter $refundConverter
     * @param LoggerInterface $logger
     * @param Config $config
     */
    public function __construct(
        Connection $connection,
        ResursbankPaymentHistoryRepository $paymentHistoryRepository,
        RefundConverter $refundConverter,
        LoggerInterface $logger,
        Config $config
    ) {
        $this->connection = $connection;
        $this->paymentHistoryRepository = $paymentHistoryRepository;
        $this->logger = $logger;
        $this->refundConverter = $refundConverter;
        $this->config = $config;

        // Resolve employee name from context.
        $employee = Context::getContext()->employee;
        $this->employeeName = $employee instanceof Employee ?
            (string) $employee->firstname . ' ' . (string) $employee->lastname :
            'Unknown Admin';
    }

    /**
     * @param object $order
     *
     * @return bool
     */
    public function isResursOrder(object $order): bool
    {
        return
            isset($order->module) &&
            strpos($order->module, 'psrb') === 0
        ;
    }

    /**
     * @param Order $order
     *
     * @return void
     *
     * @throws ORMException
     * @throws PaymentException
     * @throws JsonException
     * @throws ShopException
     */
    public function capture(Order $order): void
    {
        if (!$this->isEnabled($order)) {
            return;
        }
        // Resolve data from command subject.
        $paymentId = $order->reference;

        /* @noinspection BadExceptionsProcessingInspection */
        try {
            // Establish API connection.
            $connection = $this->connection->getConnection(
                $this->connection->getCredentialsFromOrder($order)
            );

            // Log command being called.
            $this->paymentHistoryRepository->create(
                 $order->id,
                ResursbankPaymentHistory::EVENT_CAPTURE_CALLED,
                $this->employeeName
            );

            if ($connection->canDebit($paymentId)) {
                // Log API method being called.
                $this->paymentHistoryRepository->create(
                    $order->id,
                    ResursbankPaymentHistory::EVENT_CAPTURE_API_CALLED,
                    $this->employeeName
                );

                // Capture payment.
                $connection->finalizePayment($paymentId);
            }

            // Close transaction when order is paid in full.
        } catch (Exception $e) {
            // Log error.
            $this->logger->exception($e);
            $this->paymentHistoryRepository->create(
                $order->id,
                ResursbankPaymentHistory::EVENT_CAPTURE_FAILED,
                $this->employeeName
            );

            // Pass safe error upstream.
            throw new PaymentException('Failed to capture payment.');
        }
    }

    /**
     * @param Order $order
     * @param array $orderDetails
     *
     * @return void
     *
     * @throws JsonException
     * @throws ORMException
     * @throws PaymentException
     * @throws ShopException
     */
    public function cancel(Order $order, array $orderDetails = []): void
    {
        if (!$this->isEnabled($order)) {
            return;
        }
        $paymentId = $order->reference;

        try {
            // Establish API connection.
            $connection = $this->connection->getConnection(
                $this->connection->getCredentialsFromOrder($order)
            );

            // Log command being called.
            $this->paymentHistoryRepository->create(
                $order->id,
                ResursbankPaymentHistory::EVENT_CANCEL_CALLED,
                $this->employeeName
            );

            if ($connection->canAnnul($paymentId)) {
                // Log API method being called.
                $this->paymentHistoryRepository->create(
                    $order->id,
                    ResursbankPaymentHistory::EVENT_CANCEL_API_CALLED,
                    $this->employeeName
                );

                // Add items to API payload.
                if (!empty($orderDetails)) {
                    $this->addOrderLines(
                        $connection,
                        $orderDetails
                    );
                }

                // Cancel payment.
                $connection->annulPayment($paymentId);
            }
        } catch (Exception $e) {
            // Log error.
            $this->logger->exception($e);
            $this->paymentHistoryRepository->create(
                $order->id,
                ResursbankPaymentHistory::EVENT_CANCEL_FAILED,
                $this->employeeName
            );

            // Pass safe error upstream.
            throw new PaymentException('Failed to cancel payment.');
        }
    }

    /**
     * @param Order $order
     * @param array<AbstractItem> $orderDetails
     *
     * @return void
     *
     * @throws PaymentException
     * @throws ORMException
     * @throws JsonException
     * @throws ShopException
     */
    public function refund(Order $order, array $orderDetails = []): void
    {
        if (!$this->isEnabled($order)) {
            return;
        }
        $paymentId = $order->reference;

        try {
            // Establish API connection.
            $connection = $this->connection->getConnection(
                $this->connection->getCredentialsFromOrder($order)
            );

            // Log command being called.
            $this->paymentHistoryRepository->create(
                $order->id,
                ResursbankPaymentHistory::EVENT_REFUND_CALLED,
                $this->employeeName
            );

            if ($connection->canCredit($paymentId)) {
                // Log API method being called.
                $this->paymentHistoryRepository->create(
                    $order->id,
                    ResursbankPaymentHistory::EVENT_REFUND_API_CALLED,
                    $this->employeeName
                );

                // Add items to API payload.
                if (is_array($orderDetails) && !count($orderDetails)) {
                    $this->addOrderLines(
                        $connection,
                        $this->refundConverter->convert($order)
                    );
                } else {
                    $this->addOrderLines(
                        $connection,
                        $orderDetails
                    );
                }

                $connection->setGetPaymentMatchKeys(['artNo', 'description', 'unitMeasure']);
                // Refund payment.
                $connection->creditPayment($paymentId, null, false, true);
            }
        } catch (Exception $e) {
            // Log error.
            $this->logger->exception($e);
            $this->paymentHistoryRepository->create(
                $order->id,
                ResursbankPaymentHistory::EVENT_REFUND_FAILED,
                $this->employeeName
            );

            // Exceptions thrown in admin backend should be "customer safe" as this is actually based on API errors
            // when crediting payments.
            throw new PaymentException(
                sprintf(
                    'Failed to refund payment: %s.',
                    $this->getSoapFaultByException($e)
                )
            );
        }
    }

    /**
     * Extract faultString from a recursive exception (requires ExceptionHandler from netCurl).
     *
     * @param ResursException $e
     *
     * @return string
     *
     * @since 1.0.0
     * @todo Centralize method since it also exists in the simplified module.
     */
    private function getSoapFaultByException(ResursException $e): string
    {
        $return = $e->getMessage();
        $previousException = $e->getPrevious();

        if ($previousException instanceof ExceptionHandler) {
            if ($previousException->getCode() < 500 || $previousException->getCode() >= 600) {
                // Previous exceptions may be quite ugly when it comes to
                // internal errors, which is not the proper way to handle
                // errors that comes from a soapFault. We will skip the message
                // if the error code is covered by 5XX. If the code is higher,
                // something else may want to tell us something.
                $return = $previousException->getMessage();
            }
            if (method_exists($previousException, 'getPrevious')) {
                $recursedPreviousException = $previousException->getPrevious();

                if ($recursedPreviousException instanceof SoapFault &&
                    isset($recursedPreviousException->faultstring)
                ) {
                    $return = $recursedPreviousException->faultstring;
                    // Fail over recursively if possible, since userErrorMessage
                    // details are lesser than the faultString.
                    if (isset($recursedPreviousException->detail->ECommerceError->userErrorMessage)) {
                        $return = $recursedPreviousException
                            ->detail
                            ->ECommerceError
                            ->userErrorMessage;
                    }
                }
            } else {
                $return = $previousException->getMessage();
            }
        }

        return $return;
    }

    /**
     * Use the addOrderLine method in ECom to add payload data while avoiding
     * methods that override supplied data.
     *
     * @param ResursBank $connection
     * @param array<Item> $data
     *
     * @throws Exception
     */
    private function addOrderLines(
        ResursBank $connection,
        array $data
    ): void {
        foreach ($data as $item) {
            $connection->addOrderLine(
                $item->getArtNo(),
                $item->getDescription(),
                Tools::ps_round($item->getUnitAmountWithoutVat(), Context::getContext()->getComputingPrecision()),
                $item->getVatPct(),
                $item->getUnitMeasure(),
                $item->getType(),
                $item->getQuantity()
            );
        }
    }

    /**
     * Whether after shop is enabled.
     *
     * @param Order $order
     *
     * @return bool
     *
     * @throws ShopException
     */
    protected function isEnabled(Order $order): bool
    {
        $shop = new ShopConstraint(
            (int) $order->id_shop,
            (int) $order->id_shop_group
        );

        return (bool) $this->config->isEnabled($shop);
    }
}
