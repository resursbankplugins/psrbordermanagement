<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Observer;

use Doctrine\ORM\ORMException;
use Exception;
use InvalidArgumentException;
use JsonException;
use OrderState as PsOrderState;
use PrestaShop\PrestaShop\Adapter\Entity\Configuration;
use PrestaShop\PrestaShop\Adapter\Entity\Order;
use PrestaShop\PrestaShop\Adapter\Entity\OrderState;
use PrestaShop\PrestaShop\Core\Domain\Order\Payment\Exception\PaymentException;
use PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException;
use PrestaShopBundle\Service\Routing\Router;
use PrestaShopDatabaseException;
use PrestaShopException;
use psrbordermanagement;
use Resursbank\Core\Api\Connection;
use Resursbank\Core\Api\Credentials;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\OrderManagement\Config\Config;
use Resursbank\OrderManagement\Service\OrderManagement;

/**
 * Add buttons on the order view to execute AfterShop actions (capture, refund
 * and cancel).
 */
class OrderStateUpdate extends AbstractObserver
{
    /**
     * @var OrderManagement
     */
    private $orderManagement;

    /**
     * @param LoggerInterface $logger
     * @param Credentials $credentials
     * @param Config $config
     * @param Router $router
     * @param OrderManagement $orderManagement
     * @param Connection $connection
     */
    public function __construct(
        LoggerInterface $logger,
        Credentials $credentials,
        Config $config,
        Router $router,
        OrderManagement $orderManagement,
        Connection $connection
    ) {
        parent::__construct($logger, $credentials, $config, $router);

        $this->orderManagement = $orderManagement;
    }

    /**
     * Execute After Shop methods.
     *
     * @param psrbordermanagement $module
     * @param array $params
     *
     * @return void
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws ORMException
     * @throws JsonException
     * @throws PaymentException
     * @throws ShopException
     */
    public function execute(psrbordermanagement $module, array $params): void
    {
        // Validate anonymous data in $params.
        $this->validateParams($params);

        $order = new Order((int) $params['id_order']);

        /** @var OrderState $newOrderState */
        $newOrderState = $params['newOrderStatus'];

        if ($newOrderState->paid) {
            $this->orderManagement->capture($order);
        } elseif ((int) $newOrderState->id === (int) _PS_OS_CANCELED_) {
            $this->orderManagement->cancel($order);
        } elseif ((int) $newOrderState->id === (int) _PS_OS_REFUND_) {
            $this->orderManagement->refund($order);
        }
    }

    /**
     * @param array $params
     *
     * @return void
     */
    private function validateParams(array $params): void
    {
        if (!isset($params['id_order']) || (int) $params['id_order'] === 0) {
            throw new InvalidArgumentException('No order id supplied.');
        }

        if (!isset($params['newOrderStatus']) ||
            !($params['newOrderStatus'] instanceof PsOrderState)
        ) {
            throw new InvalidArgumentException('No new order state found');
        }
    }
}
