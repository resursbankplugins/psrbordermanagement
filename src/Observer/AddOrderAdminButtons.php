<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Observer;

use Exception;
use InvalidArgumentException;
use PrestaShop\PrestaShop\Adapter\Entity\Order;
use PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use PrestaShopBundle\Controller\Admin\Sell\Order\ActionsBarButton;
use PrestaShopBundle\Controller\Admin\Sell\Order\ActionsBarButtonsCollection;
use PrestaShopBundle\Service\Routing\Router;
use PrestaShopDatabaseException;
use PrestaShopException;
use psrbordermanagement;
use Resursbank\Core\Api\Connection;
use Resursbank\Core\Api\Credentials;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\OrderManagement\Config\Config;
use Resursbank\OrderManagement\Service\OrderManagement;

/**
 * Add buttons on the order view to execute AfterShop actions (capture, refund
 * and cancel).
 */
class AddOrderAdminButtons extends AbstractObserver
{
    /**
     * @var ActionsBarButtonsCollection
     */
    private $buttons;

    /**
     * @var OrderManagement
     */
    private $orderManagement;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param LoggerInterface $logger
     * @param Credentials $credentials
     * @param Config $config
     * @param Router $router
     * @param OrderManagement $orderManagement
     * @param Connection $connection
     */
    public function __construct(
        LoggerInterface $logger,
        Credentials $credentials,
        Config $config,
        Router $router,
        OrderManagement $orderManagement,
        Connection $connection
    ) {
        parent::__construct($logger, $credentials, $config, $router);

        $this->orderManagement = $orderManagement;
        $this->connection = $connection;
    }

    /**
     * @param psrbordermanagement $module
     * @param array $params
     *
     * @return void
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws InvalidArgumentException
     * @throws ShopException
     */
    public function execute(psrbordermanagement $module, array $params): void
    {
        // Validate anonymous data in $params.
        $this->validateParams($params);

        $order = new Order((int) $params['id_order']);

        if ($this->isEnabled($order)) {
            $this->buttons = $params['actions_bar_buttons_collection'];
            $this->addCaptureButton($order, $module);
        }
    }

    /**
     * @param array $params
     *
     * @return void
     */
    private function validateParams(array $params): void
    {
        if (!isset($params['id_order']) || (int) $params['id_order'] === 0) {
            throw new InvalidArgumentException('No order id supplied.');
        }

        if (!isset($params['actions_bar_buttons_collection']) ||
            !($params['actions_bar_buttons_collection'] instanceof ActionsBarButtonsCollection)
        ) {
            throw new InvalidArgumentException('No action bar buttons.');
        }
    }

    /**
     * Append button to execute capture command to action bar.
     *
     * @param Order $order
     * @param psrbordermanagement $module
     * @return void
     */
    private function addCaptureButton(
        Order $order,
        psrbordermanagement $module
    ): void {
        try {
            $button = new ActionsBarButton(
                'btn-action',
                [
                    'onClick' => sprintf(
                        "window.location = '%s'",
                        $this->router->generate(
                            'resursbank_ordermanagement_admin_order_capture',
                            ['orderId' => $order->id]
                        )
                    ),
                ],
                sprintf(
                    "<img src='%s' alt='' /> Capture",
                    $module->getPathUri() . 'views/img/button_logo.png'
                )
            );
            $this->buttons->add($button);
        } catch (Exception $e) {
            $this->logger->error('Unable to add Capture button.');
            $this->logger->exception($e);
        }
    }

    /**
     * Whether this observer may execute.
     *
     * @throws ShopException
     * @throws InvalidDataException
     * @throws Exception
     */
    private function isEnabled(
        Order $order
    ): bool {
        $result = false;

        $shop = new ShopConstraint(
            (int)$order->id_shop,
            (int)$order->id_shop_group
        );

        if ($this->config->isEnabled($shop) &&
            $this->orderManagement->isResursOrder($order)
        ) {
            $connection = $this->connection->getConnection(
                $this->connection->getCredentialsFromOrder($order)
            );

            $result = (
                $connection->canDebit($order->reference) &&
                !$connection->getIsDebited($order->reference)
            );
        }

        return $result;
    }
}
