# Resurs Bank - PrestaShop module - Order Management

## Description

Functionality to interact with payments at Resurs Bank.

---

## Prerequisites

* [PrestaShop 1.7.7+]

---

#### 1.0.0

* Initial release.
