CREATE TABLE IF NOT EXISTS `PREFIX_resursbank_payment_history` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Prim Key',
    `order_id` int(10) unsigned NOT NULL COMMENT 'Order reference.',
    `event` varchar(255) NOT NULL COMMENT 'Event that was triggered.',
    `user` varchar(255) NOT NULL COMMENT 'User that triggered the event.',
    `extra` text DEFAULT NULL COMMENT 'Additional information about what happened.',
    `state_from` varchar(255) DEFAULT NULL COMMENT 'What state the payment went from.',
    `state_to` varchar(255) DEFAULT NULL COMMENT 'What state the payment went to.',
    `created_at` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Created At',
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_RB_HISTORY_ORDER` FOREIGN KEY (`order_id`) REFERENCES `ps_orders` (`id_order`) ON DELETE CASCADE
) CHARSET=utf8 COMMENT='Resurs Bank Payment History Table'
