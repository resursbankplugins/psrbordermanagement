<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use Resursbank\OrderManagement\Controller\FrontController;

/**
 * Integrates the UPDATE callback dispatched by the Resurs Banks API.
 */
class psrbordermanagementUpdateModuleFrontController extends FrontController
{
    /**
     * @throws Exception
     *
     * @see FrontController::postProcess()
     * @since 1.0.0
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function postProcess(): void
    {
        try {
            $this->getCallbackService()->update(
                $this->getPaymentId(),
                $this->getDigest()
            );
            $this->successResponse();
        } catch (Exception $e) {
            $this->getLoggerService()->exception($e);
            $this->failedResponse($e->getMessage(), $e->getCode());
        }
    }
}
