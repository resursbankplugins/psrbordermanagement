<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

use Resursbank\OrderManagement\Controller\Callback;
use Resursbank\OrderManagement\Controller\FrontController;

/**
 * Integrates the TEST callback dispatched by the Resurs Banks API.
 */
class psrbordermanagementTestModuleFrontController extends FrontController
{
    /**
     * @throws Exception
     *
     * @see FrontController::postProcess()
     * @since 1.0.0
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function postProcess(): void
    {
        /** @var callback $callbackService */
        $callbackService = $this->get(
            'resursbank.ordermanagement.controller.callback'
        );

        try {
            $callbackService->test();
        } catch (Exception $e) {
            $this->failedResponse($e->getMessage(), $e->getCode());

            return;
        }

        $this->successResponse();
    }
}
