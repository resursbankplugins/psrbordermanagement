<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

/* Start our custom atuoloader. We must start it at this point for the module
 manager to function. */
require_once(_PS_MODULE_DIR_ . "/psrbcore/src/Service/Autoloader.php");

use PrestaShop\PrestaShop\Adapter\Entity\Configuration;
use PrestaShop\PrestaShop\Adapter\Entity\OrderState;
use Resursbank\Core\Exception\InstallerException;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Core\Exception\MissingPathException;
use Resursbank\Core\Logger\Logger;
use Resursbank\Core\ModuleInterface;
use Resursbank\Core\Service\Sql;
use Resursbank\Core\Service\Tab as ResursTab;
use Resursbank\Core\Traits\Module\Init;
use Resursbank\Core\Traits\Module\Message;
use Resursbank\Core\Traits\Module\OrderState as OrderStateTrait;
use Resursbank\Core\Traits\Module\Validate;
use Resursbank\OrderManagement\Config\Config;
use Resursbank\OrderManagement\Model\OrderStates;
use Resursbank\OrderManagement\Observer\AddOrderAdminButtons;
use Resursbank\OrderManagement\Observer\BeforeUpdateCancelProduct;
use Resursbank\OrderManagement\Observer\OrderStateUpdate;
use Resursbank\OrderManagement\Repository\ResursbankPaymentHistoryRepository;
use Resursbank\OrderManagement\Service\PaymentInfo;
use TorneLIB\Exception\ExceptionHandler;

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Order management module main class.
 */
class psrbordermanagement extends Module implements ModuleInterface
{
    use Init;
    use Validate;
    use OrderStateTrait;
    use Message;

    /**
     * List of hooks (event observers) to register.
     *
     * @var string[]
     */
    private $hooks = [
        'actionBeforeUpdateCancelProductFormHandler',
        'displayAdminOrderTabLink',
        'displayAdminOrderTabContent',
        'actionOrderStatusPostUpdate',
        'actionGetAdminOrderButtons',
        'displayAdminOrderSide',
        'actionAdminControllerSetMedia',
    ];

    /**
     * @throws ExceptionHandler
     * @throws ReflectionException
     * @throws MissingPathException
     */
    public function __construct()
    {
        $this->init($this, 1);
        $this->confirmUninstall = $this->l(
            'Note that if you uninstall this module your orders will not ' .
            'automatically update from events from Resurs Bank'
        );
        parent::__construct();
    }

    /**
     * @throws InstallerException
     * @throws Exception
     */
    public function install(): bool
    {
        try {
            // This module cannot function without psrbcore.
            $this->validateCore();

            if (!parent::install()) {
                throw new Exception('Parent installation procedure failed.');
            }

            // Install tables.
            Sql::exec('psrbordermanagement', 'install.sql');

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);

            // Config page tab.
            $tab->create(
                $this->getName(),
                'ResursBankOrderManagement',
                'ResursBank',
                'Order Management',
                'resursbank_ordermanagement_admin_config',
                'Modules.ResursBank.OrderManagement.Tab'
            );

            // Disable module by default.
            Configuration::updateValue(Config::ENABLED, '1');

            // Install custom order states.
            $this->installOrderStates($this, OrderStates::states());

            // Assign default order state mappings.
            Configuration::updateValue(
                Config::PENDING_ORDER_STATE,
                (int) Configuration::get(OrderStates::PAYMENT_REVIEW)
            );

            Configuration::updateValue(
                Config::PROCESSING_ORDER_STATE,
                (int) Configuration::get(OrderStates::PAYMENT_PROCESSING)
            );

            Configuration::updateValue(
                Config::COMPLETED_ORDER_STATE,
                (int) Configuration::get('PS_OS_PAYMENT')
            );

            Configuration::updateValue(
                Config::ANNULLED_ORDER_STATE,
                (int) Configuration::get('PS_OS_CANCELED')
            );

            Configuration::updateValue(
                Config::CREDITED_ORDER_STATE,
                (int) Configuration::get('PS_OS_REFUND')
            );
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function uninstall(): bool
    {
        try {
            if (!parent::uninstall()) {
                throw new Exception('Parent uninstall procedure failed.');
            }

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);
            $tab->delete('ResursBankOrderManagement');

            // Delete config data.
            Configuration::deleteByName(Config::ENABLED);
            Configuration::deleteByName(Config::PENDING_ORDER_STATE);
            Configuration::deleteByName(Config::PROCESSING_ORDER_STATE);
            Configuration::deleteByName(Config::COMPLETED_ORDER_STATE);
            Configuration::deleteByName(Config::ANNULLED_ORDER_STATE);
            Configuration::deleteByName(Config::CREDITED_ORDER_STATE);
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @param bool $force_all
     *
     * @return bool
     *
     * @throws InstallerException
     */
    public function enable($force_all = false): bool
    {
        try {
            // This module cannot function without psrbcore.
            $this->validateCore();

            if (!parent::enable($force_all)) {
                throw new Exception('Parent enable procedure failed.');
            }

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);
            $tab->activate('ResursBankOrderManagement');

            // Register hooks (event observers).
            $this->registerHook($this->hooks);
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @param bool $force_all
     *
     * @return bool
     *
     * @throws InstallerException
     */
    public function disable($force_all = false): bool
    {
        try {
            if (!parent::disable($force_all)) {
                throw new Exception('Parent disable procedure failed.');
            }

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);
            $tab->deactivate('ResursBankOrderManagement');

            // Unregister hooks (event observers).
            foreach ($this->hooks as $hook) {
                $this->unregisterHook($hook);
            }
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * Adds payment history tab link to order view.
     *
     * $params = ['id_order' => orderId]
     *
     * @param array $params
     *
     * @return string|null
     * @noinspection PhpUnused
     *
     * @throws PrestaShopException
     */
    public function hookDisplayAdminOrderTabLink(array $params): ?string
    {
        return $this->renderPaymentHistory($params, 'link');
    }

    /**
     * Adds payment history tab content to order view.
     *
     * $params = ['id_order' => orderId]
     *
     * @param array $params
     *
     * @return string|null
     * @noinspection PhpUnused
     *
     * @throws PrestaShopException
     */
    public function hookDisplayAdminOrderTabContent(array $params): ?string
    {
        return $this->renderPaymentHistory($params, 'content');
    }

    /**
     * @throws PrestaShopException
     */
    private function renderPaymentHistory(
        array $params,
        string $template
    ): ?string {
        $result = null;

        try {
            /** @var ResursbankPaymentHistoryRepository $historyRepository */
            $historyRepository = $this->get(
                'resursbank.ordermanagement.payment_history.repository'
            );

            $histories = $historyRepository->getList((int) $params['id_order']);

            if (count($histories) > 0) {
                $result = $this->get('twig')->render(
                    "@Modules/psrbordermanagement/views/templates/order/payment_history_tab_${template}.twig",
                    [
                        'paymentHistories' => $histories,
                        'paymentHistoryTitle' => $this->l(
                            'Payment History'
                        ),
                    ]
                );
            }
        } catch (Exception $e) {
            $this->getLog()->exception($e);
        }

        return $result;
    }

    /**
     * Reflect partial refund to payment at Resurs Bank.
     *
     * $params = ['id' => orderId, 'form_data' => FormDataFromAdmin]
     *
     * @param array $params
     *
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function hookActionBeforeUpdateCancelProductFormHandler(
        array $params
    ): void {
        try {
            /** @var BeforeUpdateCancelProduct $beforeUpdateCancelProduct */
            $observer = $this->get(
                'resursbank.ordermanagement.observer.admin.order.before.update.cancel.product'
            );

            if (!$observer instanceof BeforeUpdateCancelProduct) {
                throw new RuntimeException(
                    'Failed resolving partial refund observer from container.'
                );
            }

            $observer->execute($this, $params);
        } catch (Exception $e) {
            $this->getLog()->exception($e);

            try {
                $this->addErrorMessage($this, $this->l(
                    'Failed to update the payment at Resurs Bank. Manual ' .
                    'action through Resurs Merchant Portal is suggested!'
                ));
                $this->addErrorMessage($this, $e->getMessage());
            } catch (Exception $e) {
                $this->getLog()->exception($e);
            }
        }
    }

    /**
     * Display after shop action buttons on order view.
     *
     * $params = [
     *     'id_order' => orderId,
     *     'actions_bar_buttons_collection'=>\PrestaShopBundle\Controller\Admin\Sell\Order\ActionsBarButtonsCollection,
     *     'controller' => \PrestaShopBundle\Controller\Admin\Sell\Order\OrderController
     * ]
     *
     * @param array $params
     *
     * @throws PrestaShopException
     */
    public function hookActionGetAdminOrderButtons(array $params): void
    {
        try {
            /** @var AddOrderAdminButtons $addOrderButtonsObserver */
            $addOrderButtonsObserver = $this->get(
                'resursbank.ordermanagement.observer.admin.order.buttons'
            );

            if (!$addOrderButtonsObserver) {
                return;
            }
            $addOrderButtonsObserver->execute($this, $params);
        } catch (Exception $e) {
            $this->getLog()->exception($e);
        }
    }

    /**
     * Execute After Shop methods when status is updated in backoffice.
     *
     * $params = [
     *     'id_order' => orderId,
     *     'newOrderStatus'=> \PrestaShop\PrestaShop\Adapter\Entity\OrderState,
     * ]
     *
     * @param array $params
     * @noinspection PhpUnused
     *
     * @throws PrestaShopException
     */
    public function hookActionOrderStatusPostUpdate(array $params): void
    {
        try {
            if ($this->context->controller->controller_type === 'admin' ||
                $this->context->controller->controller_type === 'moduleadmin'
            ) {
                /** @var OrderStateUpdate $observer */
                $observer = $this->get(
                    'resursbank.ordermanagement.observer.admin.order.state.update'
                );

                if (!$observer instanceof OrderStateUpdate) {
                    throw new RuntimeException(
                        'Failed resolving order state update observer from container.'
                    );
                }

                $observer->execute($this, $params);
            }
        } catch (Exception $e) {
            $this->getLog()->exception($e);

            try {
                $this->addErrorMessage($this, $this->l(
                    'Failed to update the payment at Resurs Bank. Manual ' .
                    'action through Resurs Merchant Portal is suggested!'
                ));
                $this->addErrorMessage($this, $e->getMessage());
            } catch (Exception $e) {
                $this->getLog()->exception($e);
            }
        }
    }

    /**
     * @throws PrestaShopException
     * @noinspection PhpUnused
     */
    public function hookDisplayAdminOrderSide(array $params): string
    {
        $result = '';

        try {
            $orderId = isset($params['id_order']) ? (int) $params['id_order'] : 0;

            if ($orderId === 0) {
                throw new InvalidDataException('Invalid order id');
            }

            $order = new Order($orderId);

            if ($order->reference === '') {
                throw new InvalidDataException("Failed to resolve order ${orderId}");
            }

            /** @var PaymentInfo $view */
            $view = $this->get('resursbank.ordermanagement.service.payment.info');

            $result = $view->render($order);
        } catch (Exception $e) {
            $this->getLog()->exception($e);
        }

        return $result;
    }

    /**
     * Add address fetching and phone validation JS too checkout.
     *
     * @return void
     *
     * @throws Exception
     */
    public function hookActionAdminControllerSetMedia(): void
    {
        $self = $this->context->controller->php_self;

        if ($self === 'AdminOrders') {
            $this->context->controller->addCss(
                $this->getPathUri() . '/css/payment-info.css',
            );
        }
    }

    /**
     * @return string
     *
     * @since 1.0.0
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @return string
     *
     * @since 1.0.0
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Declares we intend to use the new interface to translate our module.
     *
     * @return bool
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function isUsingNewTranslationSystem(): bool
    {
        return true;
    }

    /**
     * @return Logger
     *
     * @throws PrestaShopException
     */
    private function getLog(): Logger
    {
        try {
            $logger = $this->get('resursbank.ordermanagement.logger');

            if (!$logger instanceof Logger) {
                throw new PrestaShopException('Failed to resolve logger.');
            }
        } catch (Exception $e) {
            throw new PrestaShopException($e->getMessage());
        }

        return $logger;
    }
}
